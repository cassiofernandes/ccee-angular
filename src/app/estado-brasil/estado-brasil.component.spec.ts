import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoBrasilComponent } from './estado-brasil.component';

describe('EstadoBrasilComponent', () => {
  let component: EstadoBrasilComponent;
  let fixture: ComponentFixture<EstadoBrasilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoBrasilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoBrasilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
