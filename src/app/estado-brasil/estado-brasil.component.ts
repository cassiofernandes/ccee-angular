import { Component, OnInit } from '@angular/core';
import { EstadosBrasilService } from '../shared/estados-brasil.service';
import { IEstados } from '../models/estadosBR.model';

@Component({
  selector: 'app-estado-brasil',
  templateUrl: './estado-brasil.component.html',
  styleUrls: ['./estado-brasil.component.css']
})
export class EstadoBrasilComponent implements OnInit {

  estados: IEstados[];

  constructor(
    private serveEstado: EstadosBrasilService
  ) { }

  ngOnInit() {
    this.serveEstado.getEstadoBr().subscribe(data => this.estados = data);
  }

}
