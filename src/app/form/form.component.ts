import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  formulario: FormGroup;

  constructor(
    private formBilder: FormBuilder
  ) { }

  ngOnInit() {
    this.formulario = this.formBilder.group({
      nome: [null],
      email: [null]
    })

    
  }

  onSubmit(){
    console.log(this.formulario.value);
   
  }  
}
