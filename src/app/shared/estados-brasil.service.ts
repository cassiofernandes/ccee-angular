import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IEstados } from '../models/estadosBR.model';

@Injectable({
  providedIn: 'root'
})
export class EstadosBrasilService {

  constructor(
    private http: HttpClient
  ) { }

  getEstadoBr(){
    return this.http.get<IEstados[]>('assets/dados/estadosBr.json');
  }
}
