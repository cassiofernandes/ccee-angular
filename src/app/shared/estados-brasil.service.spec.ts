import { TestBed } from '@angular/core/testing';

import { EstadosBrasilService } from './estados-brasil.service';

describe('EstadosBrasilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstadosBrasilService = TestBed.get(EstadosBrasilService);
    expect(service).toBeTruthy();
  });
});
